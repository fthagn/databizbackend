from rest_framework import serializers
from .models import Reading, Device


class ReadingSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Reading
        fields = ['timestamp', 'value']

class DeviceSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Device
        fields = ['token']
