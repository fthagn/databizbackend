from django.apps import AppConfig


class EnergydataConfig(AppConfig):
    name = 'energydata'
