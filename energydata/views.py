from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status

import json
from datetime import datetime

from .serializers import ReadingSerializer#, DeviceSerializer
from .models import Reading

# from django.conf import settings

# import firebase_admin
# from firebase_admin import credentials
# from firebase_admin import messaging

@api_view(['POST'])
def new_reading(request):
  try:
    print("new reading")
    print(request.data)

    serializer = ReadingSerializer(data=request.data)
    if not serializer.is_valid():
      raise Exception(serializer.errors)

    print("serializer valid")
    serializer.save(user=request.user)

    print("saved")
    return Response("saved", status=status.HTTP_201_CREATED)

  except Exception as e:
    print(e)


@api_view(['GET'])
def get_energy_data(request):
    try:
        print("energy data requested")
        user = request.user

        response = list(Reading.objects\
                                .filter(user=user)\
                                .values())[-(10):]

        print("returning data")
        # print(response)
        # print(json.dumps(response, default=tostr))
        return Response(json.dumps(response, default=tostr))

    except Exception as e:
        print(e)


@api_view(['GET'])
def get_notifications(request):
    try:
        print("notifications requested")
        user = request.user

        recent_data = list(Reading.objects\
                                .filter(user=user)\
                                .values())[-(10):]

        # if recent_data[-1] > (sum(recent_data[:-1]) / len(recent_data[:-1]) ):
        #   response = "Überdurchschnittlicher Verbrauch!"
        response = "dies ist eine notification"
        # last_four = []
        # hour = None
        # i = 0
        # for d in recent_data[-4:]:
        #   last_four[i] = float(d.value)
        #   i += 1

        print("returning notification")
        # print(response)
        # print(json.dumps(response, default=tostr))
        return Response(json.dumps(response, default=tostr))

    except Exception as e:
        print(e)

# @api_view(['POST'])
# def register_device(request):
#   try:
#     user = request.user
#     print(user.username, "registering device")
#
#     data = request.data
#     print("data:", data)
#     # data['user'] = user
#     # print("data:", data)
#     # serializer = DeviceSerializer(data=data, context={'request': request})
#     serializer = DeviceSerializer(data=data)
#
#     if not serializer.is_valid():
#       raise Exception(serializer.errors)
#
#     print("DeviceSerializer valid")
#     serializer.save(user=user)
#     return Response(serializer.data, status=status.HTTP_201_CREATED)
#
#   except Exception as e:
#       print("Error:", e)
#       return Response(e, status=status.HTTP_400_BAD_REQUEST)


# def send_message(device, title, body):
#   notification = messaging.Notification(title, body)
#   message = messaging.Message(notification=notification, token=device)
#   messaging.send(message)


def tostr(o):
  if isinstance(o, datetime):
    return o.strftime("%Y-%m-%d %H:%M:%S")
  else:
    return float(o)


def repr(o):
  return o.__repr__()


# cred = credentials.Certificate(json.loads(settings.FIREBASE_SERVICE_ACCOUNT_KEY))
# firebase_admin.initialize_app(cred)
