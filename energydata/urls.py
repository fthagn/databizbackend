from django.urls import path
from . import views

urlpatterns = [
    path('get', views.get_energy_data),
    path('post', views.new_reading),
    path('notify', views.get_notifications)
]
